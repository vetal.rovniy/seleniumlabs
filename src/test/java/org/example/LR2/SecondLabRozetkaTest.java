package org.example.LR2;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.Duration;

public class SecondLabRozetkaTest
{
    private WebDriver chromeDriver;
    private static final String baseUrl = "https://rozetka.com.ua/ua/";

    @BeforeClass(alwaysRun = true)
    public void setUp()
    {
        WebDriverManager.chromedriver().setup();
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--start-fullscreen");
        chromeOptions.addArguments("--headless");
        chromeOptions.addArguments("--remote-allow-origins=*");
        chromeOptions.setImplicitWaitTimeout(Duration.ofSeconds(15));
        this.chromeDriver = new ChromeDriver(chromeOptions);
    }
    @BeforeMethod
    public void preconditions()  { chromeDriver.get(baseUrl); }
    @AfterClass(alwaysRun = true)
    public void tearDown() { chromeDriver.quit(); }

    @Test
    public void onClickOnNotebooksCategory()
    {
        WebElement noteBooksButton = chromeDriver.findElement(By.xpath("/html/body/app-root/div/div/rz-main-page/div/main/rz-main-page-content/rz-app-fat-menu-tablet/nav/ul/li[1]/a"));
        Assert.assertNotNull(noteBooksButton);
        noteBooksButton.click();
        Assert.assertEquals(chromeDriver.getCurrentUrl(), baseUrl);
    }

    @Test
    public void testSearchField()
    {
        String searchNotebookPageURL = "lenovo-82s900uhra/p351264234/";
        chromeDriver.get(baseUrl+searchNotebookPageURL);
        WebElement searchField = chromeDriver.findElement(By.tagName("input"));
        Assert.assertNotNull(searchField);
        System.out.println(String.format("Name attribute: %s", searchField.getAttribute("name"))+
                String.format("\nID attribute %s",searchField.getAttribute("id"))+
                String.format("\nType attribute %s",searchField.getAttribute("type"))+
                String.format("\nValue attribute: %s",searchField.getAttribute("value"))+
                String.format("\nPosition: (%d;%d)",searchField.getLocation().x,searchField.getLocation().y)+
                String.format("\nSize: %dx%d",searchField.getSize().height,searchField.getSize().width)
        );
        String inputValue = "";
        searchField.sendKeys(inputValue);
        Assert.assertEquals(searchField.getText(), inputValue);
        searchField.sendKeys(Keys.ENTER);
        Assert.assertEquals(chromeDriver.getCurrentUrl(),baseUrl+searchNotebookPageURL);
    }

    @Test
    public void testTest()
    {
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--start-fullscreen");
        chromeOptions.addArguments("--headless");
        chromeOptions.addArguments("--remote-allow-origins=*");
        chromeOptions.setImplicitWaitTimeout(Duration.ofSeconds(15));
        WebDriver driver = new ChromeDriver(chromeOptions);

        // 1. Open the browser and navigate to https://lambdatest.github.io/sample-todo-app/
        driver.get("https://lambdatest.github.io/sample-todo-app/");

        // 2. Click on the "First Item" checkbox to mark it as completed
        WebElement firstItemCheckbox = driver.findElement(By.xpath("//li[contains(., 'First Item')]/input[@type='checkbox']"));
        firstItemCheckbox.click();

        // 3. Verify that the "First Item" checkbox is checked
        Assert.assertTrue(firstItemCheckbox.isSelected(), "The 'First Item' checkbox is not checked.");

        // 4. Enter a new task into the "What needs to be done?" input field
        String newTaskText = "New Task";
        WebElement newTaskInput = driver.findElement(By.id("sampletodotext"));
        newTaskInput.sendKeys(newTaskText);

        // 5. Press the Enter key to add the new task
        newTaskInput.submit();

        // 6. Verify that the new task is added to the task list
        WebElement newTaskElement = driver.findElement(By.xpath("//li[contains(., '" + newTaskText + "')]"));
        Assert.assertTrue(newTaskElement.isDisplayed(), "The new task is not added to the task list.");

        // 7. Click on the "Clear Completed" button to remove completed tasks
        WebElement clearCompletedButton = driver.findElement(By.id("addbutton"));
        clearCompletedButton.click();

        // 8. Verify that the completed task is no longer present in the task list
        Assert.assertTrue(newTaskElement.isDisplayed(), "The completed task is still present in the task list.");

        driver.quit();
    }
}

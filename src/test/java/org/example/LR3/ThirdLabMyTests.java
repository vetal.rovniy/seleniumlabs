package org.example.LR3;

import com.github.javafaker.Faker;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import org.apache.http.HttpStatus;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.Map;

import static io.restassured.RestAssured.given;

public class ThirdLabMyTests
{
    private static final String baseUrl = "https://petstore.swagger.io/v2";
    private static final String STORE = "/store", STORE_ORDER = STORE +"/order",
            STORE_ID = STORE_ORDER+ "/{orderId}",
            STORE_INVENTORY = STORE+"/inventory";
    private Integer orderId;

    @BeforeClass
    public void setup()
    {
        RestAssured.baseURI = baseUrl;
        RestAssured.defaultParser = Parser.JSON;
        RestAssured.requestSpecification = new RequestSpecBuilder().setContentType(ContentType.JSON).build();
        RestAssured.responseSpecification = new ResponseSpecBuilder().build();
    }

    @Test
    public void verifyPlaceOrder()
    {
        orderId = Integer.valueOf("2");
        Map<String,?> body = Map.of(
                "id",orderId,
                "petId",Integer.valueOf("15"),
                "quantity", Integer.valueOf("0"),
                "shipDate", Faker.instance().date().birthday(),
                "status", "placed",
                "complete", true
        );
        given().given().body(body).post(STORE_ORDER).then().statusCode(HttpStatus.SC_OK);
    }

    @Test(dependsOnMethods = "verifyPlaceOrder")
    public void verifyFindOrder()
    {

        given().given().pathParam("orderId",orderId)
                .get(STORE_ID)
                .then()
                .statusCode(HttpStatus.SC_OK);
    }

    @Test(dependsOnMethods = {"verifyPlaceOrder","verifyFindOrder"})
    public void verifyDeleteOrder()
    {
        given().given().pathParam("orderId", orderId)
                .delete(STORE_ID)
                .then()
                .statusCode(HttpStatus.SC_OK);
    }

}
